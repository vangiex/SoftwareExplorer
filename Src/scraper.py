import re
import json
import urllib as url
from bs4 import BeautifulSoup

# Online Resource
URL = "https://filehippo.com"

# Make BS OBJECT
def source(URL, query , Parser):
    src = url.request.urlopen(URL + query) # URL QUERY 
    src = BeautifulSoup(src, Parser) # BS OBJECT
    return src # Output

# Cleaning HTML File
def transform(html):
    soup = html  # create a new bs4 object from the html data loaded
    for script in soup(["script", "style"]):  # remove all javascript and stylesheet code
        script.extract()
    # get text
    text = soup.get_text()
    # break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())
    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    # drop blank lines
    text = ' '.join(chunk for chunk in chunks if chunk)
    # returning transformed text
    return str(text.encode(encoding='utf_8', errors='ignore'))


# Search Resource For Query
def search(query):
    name = "init" # Temp Variable for intermediate Processing
    value = []  # Temp Variable for intermediate Processing
    result = [] # Output object
    parser = source(URL + "/search?q=", query, "html.parser") # Get HTML
    pattern = re.compile('/download_[a-zA-z_]*' + query + '[_A-Za-z_}*[_\d+]*/') # Search For Download Links
    for link in parser.find_all('a', href=True): # Extract Links
        Match = pattern.match(link['href'])
        if Match != None:
            Match = Match.group()
            if Match not in set(value):
                value.append(Match)
                name = re.sub("", ' ', re.sub("download_", ' ', Match.split("/")[1]).replace("_", " "), re.I).strip()
                result.append({"Title": name , "Value": Match}) # Making Final List
    return json.dumps(result,sort_keys=True) # Return and Converting Result to JSON Object


def history(query):
    # getting history is site has software
    result = [] # Output object
    query = query + "history"   # Search Formatting
    parser = source(URL, query, "html.parser")  # Get HTML
    target = transform(parser).lower()  # Cleaning HTML
    logs = re.sub(" [0-9]*", ' ', re.sub("download_", ' ', query.split("/")[1]).replace("_", " "), re.I).strip()    # Extracting & Formating
    logs = re.findall(logs + " [a-z 0-9.]*[a-z-0-9]*[0-9-a-z]* released: [0-9 May|jun|jul|Jan|Feb|Mar|Apr|Aug|Sept|Oct|Nov|Dec 0-9]+", target , re.I)   #    Finding All Logs
    for l in logs:  # Making Final List
        result.append({"Title": l.split("released")[0].strip() , "Released": l.split("released: ")[1].strip()})
    return json.dumps(result,sort_keys=False) # Return and Converting Result to JSON Object


def debug(query):
    parser = source(URL + "/search?q=", query, "html.parser")
    for link in parser.find_all('a', href=True):
        print(link['href'])
