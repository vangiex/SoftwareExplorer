import sqlite3 as sql


def connect(DB):
    conn = sql.connect(DB)
    return conn


def create_db_schema(DB):
    conn = connect(DB)
    cur = conn.cursor()
    
    cur.execute('''DROP TABLE IF EXISTS software''')
    cur.execute('''DROP TABLE IF EXISTS tracker''')
    cur.execute('''DROP TABLE IF EXISTS user''')
    cur.execute('''DROP TABLE IF EXISTS history''')
    
    cur.execute('''CREATE TABLE software
            (
            sid integer primary key autoincrement,
            Title varchar(255),
            doq varchar(255) 
            )
            ''')
    
    cur.execute('''CREATE TABLE tracker
            (
            sid integer primary key,
            version varchar(255),
            Released varchar(255),
    
            FOREIGN KEY (sid)
            REFERENCES software (sid)
            ON DELETE CASCADE
            )
            ''')
    
    cur.execute('''CREATE TABLE user
            (
            uid integer primary key,
            user varchar(255),
            pass varchar(255),
            date varchar(255) 
            )
            ''')
    
    cur.execute('''CREATE TABLE history
            (
            uid integer,
            sid integer,
            date varchar(255) 
            )
            ''')
    
    
    
    
    




