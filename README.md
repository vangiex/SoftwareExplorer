>   First objective

    The system returns all the version numbers and release dates for that particular IT system
    Example, the input can be: 
                				Google Chrome v22
    			            	vlc
    
    The determination if a system is obsolete or not can be made on the basis of 2 criteria:
    
                                1) The version stand in list
    
                                2) If there are more than 5 newer versions of the IT system released.

>   Second objective

    It then returns all alternate systems that can be used in place of the inputted system 
    in this case, it can be 
            			Mozilla Firefox 
            			Microsoft Edge 
            			Apple Safari 

